# Gutenberg Toggle Menu
Add a toggle button when full screen mode is enabled in admin pages with Gutenberg editor.

## Requirements
- Require WordPress 5.0+ / Tested up to 5.6
- Require PHP 7.0

## Installation

### Manual
- Download and install the plugin using the built-in WordPress plugin installer.
- No settings necessary, it's a Plug-and-Play plugin !

### Composer
- Add the following repository source : 
```
    {
        "type": "vcs",
        "url": "https://gitlab.com/kofinorr/wordpress-plugins/gutenberg-toggle-menu.git"
    }
```
- Include `"kofinorr/gutenberg-toggle-menu": "dev-master"` in your composer file for last master's commits or a tag released.
- No settings necessary, it's a Plug-and-Play plugin !

## License
"Gutenberg Toggle Menu" is licensed under the GPLv3 or later.