<?php
/**
 * Plugin Name: Gutenberg Toggle Menu
 * Description: Add a toggle button when full screen mode is enabled in admin pages with Gutenberg editor.
 * Version: 1.0.0
 * Author: Kofinorr
 * Author URI: https://kofinorr.damien-roche.fr/
 * License: GPLv3 or later
 */

namespace KrrGutenbergToggleMenu;

/**
 * Class GutenbergToggleMenu
 *
 * @package KrrGutenbergToggleMenu
 */
class GutenbergToggleMenu
{
	/**
	 * GutenbergToggleMenu constructor.
	 */
	public function __construct()
	{
		add_action('admin_footer', [$this, 'view']);
		add_action('admin_enqueue_scripts', [$this, 'enqueueScripts'], 150);
	}

	/**
	 * Styles and scripts of the plugins
	 */
	public function enqueueScripts()
	{
		wp_enqueue_style('krr-gtm', plugin_dir_url(__FILE__) . 'css/style.css', [], false);
		wp_enqueue_script('krr-gtm', plugin_dir_url(__FILE__) . 'js/script.js', ['jquery'], false, true);
	}

	/**
	 * View of the buttons to render
	 */
	public function view()
	{
		$screen = get_current_screen();
		if ($screen->parent_base == 'edit' && $screen->id !== 'edit-page') {
			include_once plugin_dir_path(__FILE__) . 'views/button.php';
		}
	}
}

new GutenbergToggleMenu();